const express = require('express');
const router = express.Router();
const spellJson = require("../public/assets/spellList");

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'AC over THAC0 | Home' });
});

router.get('/builder', function(req, res, next) {
	res.render('builder', { title: 'AC over THAC0 | Character Builder' });
});

router.get('/spells', function(req, res, next) {
	res.render('spelllist', spellJson);
});

router.get('/diceroll', function(req, res, next) {
	res.render('diceroll', { title: 'AC over THAC0 | Dice Roller' });
});

router.get('/initiative', function(req, res, next) {
    res.render('initiative', { title: 'AC over THAC0 | Initiative' });
});

module.exports = router;
