const express = require('express');
const router = express.Router();

router.get('/dragonborn', function(req, res, next) {
    res.render('races/dragonborn', { title: 'AC over THAC0 | Dragonborn' });
});

router.get('/dwarf', function(req, res, next) {
    res.render('races/dwarf', { title: 'AC over THAC0 | Dwarf' });
});

router.get('/elf', function(req, res, next) {
    res.render('races/elf', { title: 'AC over THAC0 | Elf' });
});

router.get('/gnome', function(req, res, next) {
    res.render('races/gnome', { title: 'AC over THAC0 | Gnome' });
});

router.get('/halfling', function(req, res, next) {
    res.render('races/halfling', { title: 'AC over THAC0 | Halfling' });
});

router.get('/halforc', function(req, res, next) {
    res.render('races/halforc', { title: 'AC over THAC0 | Half Orc' });
});

router.get('/halfelf', function(req, res, next) {
    res.render('races/halfelf', { title: 'AC over THAC0 | Half Elf' });
});

router.get('/human', function(req, res, next) {
    res.render('races/human', { title: 'AC over THAC0 | Human' });
});

router.get('/tiefling', function(req, res, next) {
    res.render('races/tiefling', { title: 'AC over THAC0 | Tiefling' });
});

module.exports = router;
