const express = require('express');
const router = express.Router();

router.get('/normal', function(req, res, next) {
    res.render('initiative/normal', { title: 'AC over THAC0 | Normal Initiative' });
});

router.get('/speed-factor', function(req, res, next) {
    res.render('initiative/speed-factor', { title: 'AC over THAC0 | Speed Factor Initiative' });
});

router.get('/greyhawk', function(req, res, next) {
    res.render('initiative/greyhawk', { title: 'AC over THAC0 | Greyhawk Initiative' });
});

module.exports = router;
