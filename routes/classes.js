const express = require('express');
const router = express.Router();

router.get('/barbarian', function(req, res, next) {
    res.render('classes/barbarian', { title: 'AC over THAC0 | Barbarian' });
});

router.get('/bard', function(req, res, next) {
    res.render('classes/bard', { title: 'AC over THAC0 | Bard' });
});

router.get('/cleric', function(req, res, next) {
    res.render('classes/cleric', { title: 'AC over THAC0 | Cleric' });
});

router.get('/druid', function(req, res, next) {
    res.render('classes/druid', { title: 'AC over THAC0 | Druid' });
});

router.get('/fighter', function(req, res, next) {
    res.render('classes/fighter', { title: 'AC over THAC0 | Fighter' });
});

router.get('/monk', function(req, res, next) {
    res.render('classes/monk', { title: 'AC over THAC0 | Monk' });
});

router.get('/paladin', function(req, res, next) {
    res.render('classes/paladin', { title: 'AC over THAC0 | Paladin' });
});

router.get('/ranger', function(req, res, next) {
    res.render('classes/ranger', { title: 'AC over THAC0 | Ranger' });
});

router.get('/rogue', function(req, res, next) {
    res.render('classes/rogue', { title: 'AC over THAC0 | Rogue' });
});

router.get('/sorcerer', function(req, res, next) {
    res.render('classes/sorcerer', { title: 'AC over THAC0 | Sorcerer' });
});

router.get('/warlock', function(req, res, next) {
    res.render('classes/warlock', { title: 'AC over THAC0 | Warlock' });
});

router.get('/wizard', function(req, res, next) {
    res.render('classes/wizard', { title: 'AC over THAC0 | Wizard' });
});

module.exports = router;
