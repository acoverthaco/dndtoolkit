//Add functionality for the dice rolling
const add = function (die) {
	dice.push(die);

	genOutput();
	//Adjust Stats
	calcStats();
};

//Remove last
const removeLast = function () {
	dice.pop();

	//Update the output text
	genOutput();

	//Adjust the min, avg, and max
	calcStats();
	if (dice.length < 1)
	{
		document.getElementById('minTag').textContent = 'Min: 0';
		document.getElementById('avgTag').textContent = 'Avg: 0';
		document.getElementById('maxTag').textContent = 'Max: 0';
	}
	document.getElementById('totalTag').textContent = 'Total Roll:'

};

//Remove all
const removeAll = function () {
	dice = [];
	//Adjust the document tags
	document.getElementById('diceTotalsTag').textContent = 'Current Dice:';
	document.getElementById('totalTag').textContent = 'Total Roll:';
	document.getElementById('minTag').textContent = 'Min: 0';
	document.getElementById('avgTag').textContent = 'Avg: 0';
	document.getElementById('maxTag').textContent = 'Max: 0';
};

//Roll dice method
const rollDice = function () {
	let val = 0;

	dice.forEach(function(d) {
		//Random d value
		switch (d){
			case 4:
				val = Math.floor((Math.random() * 4) + 1);
				break;
			case 6:
				val = Math.floor((Math.random() * 6) + 1);
				break;
			case 8:
				val = Math.floor((Math.random() * 8) + 1);
				break;
			case 10:
				val = Math.floor((Math.random() * 10) + 1);
				break;
			case 12:
				val = Math.floor((Math.random() * 12) + 1);
				break;
			case 20:
				val = Math.floor((Math.random() * 20) + 1);
				break;
			case 100:
				val = Math.floor((Math.random() * 100) + 1);
				break;
			default:
				break;
		}

		//keep the total going
		rollTotal = rollTotal + val;
	});

	//Set the new total to the document
	document.getElementById('totalTag').textContent = 'Total Roll: ' + rollTotal;
	rollTotal = 0;
};

//stats helper method
const calcStats = function () {
	if (dice.length >= 1){
		let avgCount = 0;
		let maxCount = 0;

		//loop through the dice array and calculate stats
		dice.forEach(function (die) {
			//add the die roll to the min, avg, and max tags
			switch (die){
				case 4:
					avgCount += 3;
					maxCount += 4;
					break;
				case 6:
					avgCount += 4;
					maxCount += 6;
					break;
				case 8:
					avgCount += 5;
					maxCount += 8;
					break;
				case 10:
					avgCount += 6;
					maxCount += 10;
					break;
				case 12:
					avgCount += 7;
					maxCount += 12;
					break;
				case 20:
					avgCount += 11;
					maxCount += 20;
					break;
				case 100:
					avgCount += 51;
					maxCount += 100;
					break;
				default:
					avgCount = 0;
					maxCount = 0;
					break;
			}
		});

		//Set the stats
		if (dice.length >= 1)
		{
			document.getElementById('minTag').textContent = 'Min: ' + dice.length;
			document.getElementById('avgTag').textContent = 'Avg: ' + avgCount;
			document.getElementById('maxTag').textContent = 'Max: ' + maxCount;
		}
		else
		{
			document.getElementById('minTag').textContent = 'Min: 0';
			document.getElementById('avgTag').textContent = 'Avg: 0';
			document.getElementById('maxTag').textContent = 'Max: 0';
		}
	}
};

//ouput helper
const genOutput = function () {
	let run = false;
	document.getElementById('diceTotalsTag').textContent = 'Current Dice:';
	dice.forEach(function(d){
		if (run === false)
			document.getElementById('diceTotalsTag').textContent += ' d' + d;
		else
			document.getElementById('diceTotalsTag').textContent += ', d' + d;
		run = true;
	});
};
