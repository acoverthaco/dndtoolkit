let players = [];

//addNames
const addNames = () => {
	//Check that the name field and initiative is filled in
	const id = (players.length + 1);
	const name = document.getElementById('playerName').value;
	const bonus = document.getElementById('playerInitBonus').value;
	const roll = 0;

	if (!name == "" && !bonus == "") {
		addNameToList(name);

		const player = {
			id: id,
			name: name,
			bonus: bonus,
			roll: roll
		};
		players.push(player);

		document.getElementById("playerName").value = "";
		document.getElementById("playerInitBonus").value = "";
		document.getElementById("playerName").focus();
	}
}

const rollInitiative = () => {
	clearRolls();

	for (x = 0; x < players.length; x++) {
		players[x].roll = calcRoll(players[x].bonus);
	}

	sortArray();
	players.reverse();

	for (x = 0; x < players.length; x++) {
		const node = document.createElement("tr");
		node.setAttribute("id", "player" + x);
		document.getElementById("playerRolls").appendChild(node);

		appendName(x, players[x].name);
		appendRoll(x, players[x].bonus);
		appendRoll(x, players[x].roll);
	}
}

const addNameToList = (name) => {
	const nameList = document.getElementById("playerNames");

	if (nameList.innerHTML == "") {
		nameList.innerHTML = name;
	} else {
		nameList.innerHTML += ", " + name;
	}
}

const appendName = (num, name) => {
	const node = document.createElement("td");
	const textnode = document.createTextNode("Name: " + name);
	node.appendChild(textnode);
	document.getElementById("player" + num).appendChild(node);
}

const appendBonus = (num, bonus) => {
	const node = document.createElement("td");
	const textnode = document.createTextNode("Bonus: " + bonus);
	node.appendChild(textnode);
	document.getElementById("player" + num).appendChild(node);
}

const appendRoll = (num, roll) => {
	const node = document.createElement("td");
	const textnode = document.createTextNode("Roll: " + roll);
	node.appendChild(textnode);
	document.getElementById("player" + num).appendChild(node);
}

const calcRoll = (bonus) => {
	const roll = Math.ceil(Math.random() * (0 - 20) + 20);
	return roll + parseInt(bonus);
}

const clearRolls = () => {
	const container = document.getElementById("playerRolls");
	container.innerHTML = '';
}

const clearEverything = () => {
	players = [];
	document.getElementById("playerNames").innerHTML = "";
}

const sortArray = () => {
	var swapped;
	do {
		swapped = false;
		for (var i = 0; i < players.length - 1; i++) {
			if (players[i].roll > players[i + 1].roll) {
				var temp = players[i];
				players[i] = players[i + 1];
				players[i + 1] = temp;
				swapped = true;
			}
		}
	} while (swapped);
}
