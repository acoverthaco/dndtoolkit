const clearFilters = function () {
	document.getElementById('bard').checked = false;
	document.getElementById('cleric').checked = false;
	document.getElementById('druid').checked = false;
	document.getElementById('paladin').checked = false;
	document.getElementById('ranger').checked = false;
	document.getElementById('sorcerer').checked = false;
	document.getElementById('warlock').checked = false;
	document.getElementById('wizard').checked = false;
	document.getElementById('cantrip').checked = false;
	document.getElementById('level1').checked = false;
	document.getElementById('level2').checked = false;
	document.getElementById('level3').checked = false;
	document.getElementById('level4').checked = false;
	document.getElementById('level5').checked = false;
	document.getElementById('level6').checked = false;
	document.getElementById('level7').checked = false;
	document.getElementById('level8').checked = false;
	document.getElementById('level9').checked = false;
	document.getElementById('abjuration').checked = false;
	document.getElementById('conjuration').checked = false;
	document.getElementById('divination').checked = false;
	document.getElementById('enchantment').checked = false;
	document.getElementById('evocation').checked = false;
	document.getElementById('illusion').checked = false;
	document.getElementById('necromancy').checked = false;
	document.getElementById('transmutation').checked = false;
	document.getElementById('control').checked = false;
	document.getElementById('damage').checked = false;
	document.getElementById('support').checked = false;

	const table = document.getElementById('spellTable');

	for (let i = 1; i < table.rows.length; i++) {
		table.rows[i].classList.remove("filterHide");
	}
};

const filterClass = function (val, box) {
	if(document.getElementById(box).checked)
	{
		classArray.push(val);
	}
	else
	{
		var index = classArray.indexOf(val);
		if(index > -1)
		{
			classArray.splice(index,1);
		}
	}
};

const filterLevel = function (val, box) {
	if(document.getElementById(box).checked)
	{
		levelArray.push(val);
	}
	else
	{
		var index = levelArray.indexOf(val);
		if(index > -1)
		{
			levelArray.splice(index,1);
		}
	}
};

const filterSchool = function (val, box) {
	if(document.getElementById(box).checked)
	{
		schoolArray.push(val);
	}
	else
	{
		var index = schoolArray.indexOf(val);
		if(index > -1)
		{
			schoolArray.splice(index,1);
		}
	}
};

const filterClassification = function (val, box) {
	if(document.getElementById(box).checked)
	{
		typeArray.push(val);
	}
	else
	{
		var index = typeArray.indexOf(val);
		if(index > -1)
		{
			typeArray.splice(index,1);
		}
	}
};

const filterAllSpells = function () {
	let gClass, gLevel, gSchool, gType, iClass, iLevel, iSchool, iType = false;
	//check to see if the global arrays have a length.
	//if they do, set the global and item bool to false, if they don't, set to true
	const table = document.getElementById('spellTable');

	if(classArray.length === 0)
	{
		gClass = true;
		iClass = true;
	}
	if(levelArray.length === 0)
	{
		gLevel = true;
		iLevel = true;
	}
	if(schoolArray.length === 0)
	{
		gSchool = true;
		iSchool = true;
	}
	if(typeArray.length === 0)
	{
		gType = true;
		iType = true;
	}

	for (let i = 1; i < table.rows.length; i++)
	{
		table.rows[i].classList.remove("filterHide");

		for(let y of classArray)
		{
			if(table.rows[i].cells[3].innerText.includes(y))
			{
				iClass = true;
				break;
			}
		}//end level for

		for(let y of levelArray)
		{
			if(table.rows[i].cells[1].innerText.includes(y))
			{
				iLevel = true;
				break;
			}
		}//end class for

		for(let y of schoolArray)
		{
			if(table.rows[i].cells[2].innerText.includes(y))
			{
				iSchool = true;
				break;
			}
		}//end school for

		for(let y of typeArray)
		{
			if(table.rows[i].cells[10].innerText.includes(y))
			{
				iType = true;
				break;
			}
		}//end type for

		//check if all the flags are true, if they are, add to spell array
		if(!(iClass && iLevel && iSchool && iType))
		{
			table.rows[i].classList.toggle("filterHide");
		}

		//reset flags to global
		iClass = gClass;
		iLevel = gLevel;
		iSchool = gSchool;
		iType = gType;
	}//end spells for

};
