const mobileMenuTogle = () =>  {
    document.getElementById('navMenu').classList.toggle('is-active');
    document.getElementById('navBurger').classList.toggle('is-active');
};

const acc = document.getElementsByClassName("accordion");
let i;

for (i = 0; i < acc.length; i++) {
	acc[i].onclick = function() {
		this.classList.toggle("active");
		const panel = this.nextElementSibling;
		if (panel.style.maxHeight){
		panel.style.maxHeight = null;
		} else {
			panel.style.maxHeight = panel.scrollHeight + "px";
		}
	}
}
