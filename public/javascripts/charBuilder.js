const createChar = function () {
	document.getElementById('str').value = makeStat();

	document.getElementById('dex').value = makeStat();

	document.getElementById('con').value = makeStat();

	document.getElementById('int').value = makeStat();

	document.getElementById('wis').value = makeStat();

	document.getElementById('cha').value = makeStat();

	document.getElementById('race').value = setRace();

	document.getElementById('class').value = setClass();

	document.getElementById('raceSub').value = setSubRace();

	document.getElementById('background').value = setBackground();
};

const makeStat = function () {
	const dieOne = Math.ceil(Math.random() * (0 - 6) + 6);
	const dieTwo = Math.ceil(Math.random() * (0 - 6) + 6);
	const dieThree = Math.ceil(Math.random() * (0 - 6) + 6);
	const dieFour = Math.ceil(Math.random() * (0 - 6) + 6);

	const diceRoll = [dieOne, dieTwo, dieThree, dieFour];

	diceRoll.sort();

	const topRolls = diceRoll.slice(1, 4);

	return topRolls.reduce((x, y) => x + y);
};

const setRace = function () {
	const races = ["Aarakocra", "Aasimar", "Bugbear", "Changling", "Dragonborn", "Dwarf", "Elf", "Firbolg", "Genasi", "Gnome", "Goblin", "Goliath", "Halfling", "Half-elf", "Half-orc",
		"Hobgoblin", "Human", "Kenku", "Kobold", "Lizard-folk", "Orc", "Shifter", "Tabaxi", "Tiefling", "Triton", "Yuan-ti"];

	const raceDie = Math.ceil(Math.random() * (0 - (races.length - 1)) + (races.length - 1));

	return races[raceDie]
};

const setSubRace = function () {
	const race = document.getElementById('race').value;
	const subraces = [];


	switch (race) {
		case "Aasimar":
			subraces.push("Protector");
			subraces.push("Scourge");
			subraces.push("Fallen");
			break;

		case "Dragonborn":
			subraces.push("Fire-Type");
			subraces.push("Acid-Type");
			subraces.push("Lightning-Type");
			subraces.push("Poison-Type");
			subraces.push("Cold-Type");
			break;
		case "Dwarf":
			subraces.push("Hill");
			subraces.push("Mountain");
			subraces.push("Duergar");
			break;

		case "Elf":
			subraces.push("Wood");
			subraces.push("Half");
			subraces.push("Drow");
			subraces.push("Eladrin");
			break;

		case "Genasi":
			subraces.push("Air");
			subraces.push("Earth");
			subraces.push("Fire");
			subraces.push("Water");
			break;

		case "Gnome":
			subraces.push("Forest");
			subraces.push("Rock");
			subraces.push("Deep");
			break;

		case "Halfling":
			subraces.push("Lightfoot");
			subraces.push("Stout");
			subraces.push("Ghostwise");
			break;

		case "Half-Elf":
			subraces.push("Half-Wood");
			subraces.push("Half-Sun/Moon");
			subraces.push("Half-Drow");
			subraces.push("Half-Aquatic");
			subraces.push("N/A");
			break;

		case "Human":
			subraces.push("N/A");
			subraces.push("Variant");
			break;

		case "Shifter":
			subraces.push("Beasthide");
			subraces.push("Cliffwalk");
			subraces.push("Longstride");
			subraces.push("Longtooth");
			subraces.push("Razorclaw");
			subraces.push("Wildhunt");
			break;

		case "Tiefling":
			subraces.push("Feral");
			subraces.push("N/A");
			break;

		default:
			subraces.push("N/A");
	}

	const subracesDie = Math.ceil(Math.random() * (0 - (subraces.length - 1)) + (subraces.length - 1));

	return subraces[subracesDie]
};

const setClass = function () {
	const classes = ["Artificer", "Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Mystic", "Paladin", "Ranger", "Rogue", "Sorcerer", "Warlock", "Wizard"];

	const classDie = Math.ceil(Math.random() * (0 - (classes.length - 1)) + (classes.length - 1));

	return classes[classDie]
};

const setBackground = function () {
	const backgrounds = ["Acolyte", "Charlatan", "Criminal", "Entertainer", "Folk Hero", "Guild Artisan", "Hermit", "Noble", "Outlander", "Sage", "Sailor", "Soldier",
		"Urchin", "Haunted One", "City Watch", "Clan Crafter", "Cloistered Scholar", "Courtier", "Faction Agent", "Far Traveler", "Inheritor", "Knight of the Order",
		"Mercenary Veteran", "Urban Bounty Hunter", "Uthgardt", "Waterdhavian Noble"]

	const backgroundDie = Math.ceil(Math.random() * (0 - (backgrounds.length - 1)) + (backgrounds.length - 1));

	return backgrounds[backgroundDie]
};
